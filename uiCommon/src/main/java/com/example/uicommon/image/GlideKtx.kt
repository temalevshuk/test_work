package com.example.uicommon.image

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

fun ImageView.setImage(path: String?) {
    Glide.with(context)
        .load(path)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}
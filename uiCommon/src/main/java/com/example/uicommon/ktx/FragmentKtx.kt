package com.example.uicommon.ktx

import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.activity.addCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.example.common.createTempFile
import com.example.uicommon.R
import java.util.*

fun Fragment.registerForCheckPermissions(
    action: (isGranted: Boolean) -> Unit
): ActivityResultLauncher<String> = registerForActivityResult(
    ActivityResultContracts.RequestPermission()
) { isGranted ->
    action.invoke(isGranted)
}

fun Fragment.registerForTakePhotoCamera(
    action: () -> Unit
): ActivityResultLauncher<Uri> {
    return registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
        if (isSuccess) {
            action.invoke()
        }
    }
}

fun Fragment.registerForTakePhotoGallery(
    action: (uri: Uri) -> Unit
): ActivityResultLauncher<String> {
    return registerForActivityResult(ActivityResultContracts.GetContent()) { uriContent: Uri? ->
        uriContent?.let { uri ->
            action.invoke(uri)
        }
    }
}

fun Fragment.takePhotoCamera(
    result: ActivityResultLauncher<Uri>,
    previewPathAction: (path: String) -> Unit
) {
    val photo = requireContext().createTempFile("pic_${UUID.randomUUID()}.jpg")
    val photoURI: Uri = FileProvider.getUriForFile(
        requireContext(),
        "com.example.test.fileprovider",
        photo
    )
    previewPathAction.invoke(photo.absolutePath)
    result.launch(photoURI)
}

fun Fragment.takePhotoGallery(result: ActivityResultLauncher<String>) {
    result.launch("image/*")
}

fun Fragment.initToolbar(
    toolbar: Toolbar,
    needSupportBack: Boolean = true,
    onBackPressed: (() -> Unit)? = null
) {
    (requireActivity() as AppCompatActivity).apply {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(needSupportBack)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.brand_variant
                )
            )
        )//todo get from attr

        toolbar.setNavigationOnClickListener {
            onBackPressed?.invoke() ?: requireActivity().onBackPressed()
        }

        onBackPressed?.let { backPressed ->
            onBackPressedDispatcher.addCallback(this@initToolbar) {
                backPressed.invoke()
            }
        }
    }
}
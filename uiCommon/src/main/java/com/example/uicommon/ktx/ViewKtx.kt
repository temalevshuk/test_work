package com.example.uicommon.ktx

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager


fun View.hideKeyboard() {
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)?.let { imm ->
        imm.hideSoftInputFromWindow(this.windowToken, 0)
    }
}
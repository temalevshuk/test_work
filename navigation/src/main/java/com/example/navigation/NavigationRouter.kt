package com.example.navigation

interface NavigationRouter {
    fun toClientDataPage(
        clientId: Long = -1L,
        isCreateClient: Boolean = true
    )
}
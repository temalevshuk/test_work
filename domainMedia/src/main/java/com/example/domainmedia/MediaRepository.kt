package com.example.domainmedia

import android.net.Uri
import com.example.domainmedia.entity.MediaPath

interface MediaRepository {
    suspend fun getFilePathFromContentUri(uri: Uri): MediaPath
    suspend fun getRotatedMediaFilePath(path: MediaPath): MediaPath
}
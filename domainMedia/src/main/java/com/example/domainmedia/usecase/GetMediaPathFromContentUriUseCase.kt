package com.example.domainmedia.usecase

import android.net.Uri
import com.example.domainmedia.MediaRepository

class GetMediaPathFromContentUriUseCase(
    private val repository: MediaRepository
) {
    suspend operator fun invoke(uri: Uri) = repository.getFilePathFromContentUri(uri)
}
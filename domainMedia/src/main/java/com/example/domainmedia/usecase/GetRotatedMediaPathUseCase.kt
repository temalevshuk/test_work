package com.example.domainmedia.usecase

import com.example.domainmedia.MediaRepository
import com.example.domainmedia.entity.MediaPath

class GetRotatedMediaPathUseCase(
    private val repository: MediaRepository
) {
    suspend operator fun invoke(path: MediaPath) = repository.getRotatedMediaFilePath(path)
}
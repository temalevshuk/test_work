package com.example.domainmedia

import com.example.domainmedia.usecase.GetMediaPathFromContentUriUseCase
import com.example.domainmedia.usecase.GetRotatedMediaPathUseCase
import org.koin.dsl.module

val mediaDomainModule = module {
    factory { GetMediaPathFromContentUriUseCase(repository = get()) }
    factory { GetRotatedMediaPathUseCase(repository = get()) }
}
package com.example.domainmedia.entity

@JvmInline
value class MediaPath(val value: String)
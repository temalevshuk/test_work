package com.example.datamedia

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.provider.OpenableColumns
import com.example.common.createTempFile
import com.example.domainmedia.MediaRepository
import com.example.domainmedia.entity.MediaPath
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.*

internal class MediaRepositoryImpl(
    private val context: Context,
    private val contentResolver: ContentResolver
) : MediaRepository {

    override suspend fun getFilePathFromContentUri(
        uri: Uri
    ): MediaPath = withContext(Dispatchers.IO) {
        contentResolver.query(
            uri,
            arrayOf(OpenableColumns.DISPLAY_NAME),
            null,
            null,
            null
        )?.use { cursor ->
            if (cursor.moveToFirst()) {
                val name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                name.getFileByContentName(uri)?.path?.let { path -> MediaPath(path) }
                    ?: MediaPath("")
            } else MediaPath("")
        } ?: MediaPath("")
    }


    override suspend fun getRotatedMediaFilePath(
        path: MediaPath
    ): MediaPath = withContext(Dispatchers.Default) {
        val exif = ExifInterface(path.value)
        val orientation = exif.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_NORMAL
        )
        if (orientation > 0) {
            var bitmap = BitmapFactory.decodeFile(path.value)
            bitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> bitmap.rotate(90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> bitmap.rotate(180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> bitmap.rotate(270F)
                else -> bitmap
            }
            val tempFile = context.createTempFile("pic_${UUID.randomUUID()}.jpg")
            val outputStream = FileOutputStream(tempFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            MediaPath(tempFile.absolutePath)
        } else MediaPath(path.value)
    }


    private fun String.getFileByContentName(uri: Uri): File? {
        val dir = File("${context.filesDir}")
        if (dir.exists()) {
            dir.mkdir()
        }
        val file = File("${context.filesDir}/$this")
        if (file.exists().not()) {
            try {
                val inputStream = contentResolver.openInputStream(uri)
                val outputStream = FileOutputStream(file)
                var read: Int
                val bufferSize = 1024
                val buffers = ByteArray(bufferSize)
                while (inputStream!!.read(buffers).also { read = it } != -1) {
                    outputStream.write(buffers, 0, read)
                }
                inputStream.close()
                outputStream.close()
            } catch (e: Exception) {
                return null
            }
        }
        return file
    }

    private fun Bitmap.rotate(angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }
}
package com.example.datamedia.di

import android.content.ContentResolver
import android.content.Context
import com.example.datamedia.MediaRepositoryImpl
import com.example.domainmedia.MediaRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val mediaDataModule = module {
    fun provideContentResolver(applicationContext: Context): ContentResolver = applicationContext.contentResolver

    factory { provideContentResolver(androidApplication().applicationContext) }
    factory<MediaRepository> { MediaRepositoryImpl(androidContext(), get()) }
}
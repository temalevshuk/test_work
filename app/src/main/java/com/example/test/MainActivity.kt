package com.example.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.test.router.NavigationRouterModule
import org.koin.core.context.loadKoinModules

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadKoinModules(
            NavigationRouterModule.create(
                (supportFragmentManager.findFragmentById(R.id.fcvNavHost) as NavHostFragment)
                    .findNavController()
            )
        )
    }
}
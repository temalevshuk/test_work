package com.example.test.router

import androidx.core.os.bundleOf
import androidx.navigation.NavController
import com.example.test.R
import com.example.navigation.NavigationRouter
import com.example.uiclientcreateoredit.ClientDataPageFragment

internal class NavigationRouterImpl(
    private val navController: NavController
) : NavigationRouter {
    override fun toClientDataPage(
        clientId: Long,
        isCreateClient: Boolean
    ) {
        navController.navigate(
            R.id.action_root_to_client_data,
            bundleOf(
                ClientDataPageFragment.CLIENT_ID_EXTRA to clientId,
                ClientDataPageFragment.CLIENT_CREATE_EXTRA to isCreateClient
            )
        )
    }
}
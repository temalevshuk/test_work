package com.example.test.router

import androidx.navigation.NavController
import com.example.navigation.NavigationRouter
import org.koin.core.module.Module
import org.koin.dsl.module

internal object NavigationRouterModule {
    fun create(navController: NavController): Module = module {
        single<NavigationRouter> { NavigationRouterImpl(navController) }
    }
}
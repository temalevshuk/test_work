package com.example.test

import android.app.Application
import com.example.database.di.databaseModule
import com.example.dataclient.di.clientDataModule
import com.example.datamedia.di.mediaDataModule
import com.example.domainclient.clientDomainModule
import com.example.domainmedia.mediaDomainModule
import com.example.uiclientcreateoredit.clientDataViewModelModule
import com.example.uiclientmain.clientViewModelModule
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        startKoin {
            androidContext(this@App)
            modules(
                databaseModule,
                *domainModules,
                *dataModules,
                *viewModelModules
            )
        }
    }

    private val dataModules
        get() = arrayOf(clientDataModule, mediaDataModule)

    private val domainModules
        get() = arrayOf(clientDomainModule, mediaDomainModule)

    private val viewModelModules
        get() = arrayOf(clientViewModelModule, clientDataViewModelModule)

}
package com.example.domainclient.usecase

import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.ClientId

class DeleteClientUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke(clientId: ClientId) {
        repository.deleteClient(clientId)
    }
}
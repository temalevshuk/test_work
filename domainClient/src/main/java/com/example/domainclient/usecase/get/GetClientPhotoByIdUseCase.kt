package com.example.domainclient.usecase.get

import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.ClientId

class GetClientPhotoByIdUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke(clientId: ClientId) = repository.getClientPhotoById(clientId)
}
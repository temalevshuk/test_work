package com.example.domainclient.usecase

import com.example.domainclient.ClientRepository

class CreateClientUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke() = repository.createClient()
}
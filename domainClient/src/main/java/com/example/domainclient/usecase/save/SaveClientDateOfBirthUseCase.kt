package com.example.domainclient.usecase.save

import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.birthday.ClientBirthday
import org.threeten.bp.LocalDate

class SaveClientDateOfBirthUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke(
        clientId: ClientId,
        clientBirthday: ClientBirthday
    ) {
        repository.saveClientDateOfBirth(clientId, clientBirthday)
    }
}
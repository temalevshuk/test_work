package com.example.domainclient.usecase.get

import com.example.domainclient.ClientRepository

class GetClientsUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke() = repository.getClients()
}
package com.example.domainclient.usecase.save

import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.photo.ClientPhotoPath

class SaveClientPhotoUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke(
        clientId: ClientId,
        photo: ClientPhoto
    ) {
        repository.saveClientPhoto(clientId, photo)
    }
}
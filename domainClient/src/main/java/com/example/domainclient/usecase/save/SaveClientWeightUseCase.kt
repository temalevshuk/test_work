package com.example.domainclient.usecase.save

import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.weight.ClientWeight

class SaveClientWeightUseCase(
    private val repository: ClientRepository
) {
    suspend operator fun invoke(
        clientId: ClientId,
        clientWeight: ClientWeight
    ) {
        repository.saveClientWeight(clientId, clientWeight)
    }
}
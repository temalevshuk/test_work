package com.example.domainclient

import com.example.domainclient.entity.*
import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.weight.ClientWeight
import kotlinx.coroutines.flow.Flow

interface ClientRepository {
    suspend fun createClient(): ClientId
    suspend fun deleteClient(clientId: ClientId)
    suspend fun saveClientWeight(clientId: ClientId, clientWeight: ClientWeight)
    suspend fun saveClientDateOfBirth(clientId: ClientId, clientDateOfBirth: ClientBirthday)
    suspend fun saveClientPhoto(clientId: ClientId, clientPhoto: ClientPhoto)
    suspend fun getClients(): Flow<List<Client>>
    suspend fun getClientWeightById(clientId: ClientId): ClientWeight?
    suspend fun getClientDateOfBirthById(clientId: ClientId): ClientBirthday?
    suspend fun getClientPhotoById(clientId: ClientId): ClientPhoto?
}
package com.example.domainclient.entity.birthday

@JvmInline
value class ClientBirthdayId(val value: Long)
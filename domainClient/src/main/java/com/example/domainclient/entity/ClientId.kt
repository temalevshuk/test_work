package com.example.domainclient.entity

@JvmInline
value class ClientId(val value: Long)
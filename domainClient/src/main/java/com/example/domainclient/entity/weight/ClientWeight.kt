package com.example.domainclient.entity.weight

data class ClientWeight(
    val id: ClientWeightId = ClientWeightId(System.currentTimeMillis()),
    val value: String = "",
    val type: Type = Type.LB
) {
    enum class Type {
        LB, KG
    }
}
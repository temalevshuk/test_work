package com.example.domainclient.entity.birthday

import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId

data class ClientBirthday(
    val id: ClientBirthdayId = ClientBirthdayId(System.currentTimeMillis()),
    val date: LocalDate = LocalDate.now(ZoneId.systemDefault())
)
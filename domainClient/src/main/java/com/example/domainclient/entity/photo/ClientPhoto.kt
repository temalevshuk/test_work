package com.example.domainclient.entity.photo

data class ClientPhoto(
    val id: ClientPhotoPathId = ClientPhotoPathId(System.currentTimeMillis()),
    val path: ClientPhotoPath = ClientPhotoPath("")
)
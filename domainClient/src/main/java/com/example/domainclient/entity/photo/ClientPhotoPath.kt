package com.example.domainclient.entity.photo

@JvmInline
value class ClientPhotoPath(val value: String)
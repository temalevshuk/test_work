package com.example.domainclient.entity.photo

@JvmInline
value class ClientPhotoPathId(val value: Long)
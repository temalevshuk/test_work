package com.example.domainclient.entity.weight

@JvmInline
value class ClientWeightId(val value: Long)
package com.example.domainclient.entity

import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.weight.ClientWeight
import org.threeten.bp.LocalDate

data class Client(
    val id: ClientId,
    val weight: ClientWeight,
    val dateOfBirthday: ClientBirthday,
    val photo: ClientPhoto
)
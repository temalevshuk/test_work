package com.example.domainclient

import com.example.domainclient.usecase.*
import com.example.domainclient.usecase.get.GetClientDateOfBirthByIdUseCase
import com.example.domainclient.usecase.get.GetClientPhotoByIdUseCase
import com.example.domainclient.usecase.get.GetClientWeightByIdUseCase
import com.example.domainclient.usecase.get.GetClientsUseCase
import com.example.domainclient.usecase.save.SaveClientDateOfBirthUseCase
import com.example.domainclient.usecase.save.SaveClientPhotoUseCase
import com.example.domainclient.usecase.save.SaveClientWeightUseCase
import org.koin.dsl.module

val clientDomainModule = module {
    factory { GetClientsUseCase(repository = get()) }
    factory { GetClientWeightByIdUseCase(repository = get()) }
    factory { GetClientDateOfBirthByIdUseCase(repository = get()) }
    factory { GetClientPhotoByIdUseCase(repository = get()) }

    factory { CreateClientUseCase(repository = get()) }
    factory { DeleteClientUseCase(repository = get()) }

    factory { SaveClientWeightUseCase(repository = get()) }
    factory { SaveClientDateOfBirthUseCase(repository = get()) }
    factory { SaveClientPhotoUseCase(repository = get()) }
}
package com.example.uiclientcreateoredit.weight

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.weight.ClientWeight
import com.example.domainclient.usecase.get.GetClientWeightByIdUseCase
import com.example.domainclient.usecase.save.SaveClientWeightUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

internal class ClientWeightViewModel(
    private val saveClientWeightUseCase: SaveClientWeightUseCase,
    private val getClientWeightByIdUseCase: GetClientWeightByIdUseCase
) : ViewModel() {

    private val _clientWeight: MutableStateFlow<ClientWeight> = MutableStateFlow(ClientWeight())
    internal val clientWeight = _clientWeight.asStateFlow()

    fun loadClientWeight(
        clientId: ClientId
    ) {
        viewModelScope.launch {
            getClientWeightByIdUseCase.invoke(clientId)?.let { clientWeight ->
                _clientWeight.update { clientWeight }
            }
        }
    }

    fun saveClientWeight(
        clientId: ClientId,
        weightValue: String
    ) {
        viewModelScope.launch {
            _clientWeight.update { clientWeight -> clientWeight.copy(value = weightValue) }
            saveClientWeightUseCase.invoke(clientId, _clientWeight.value)
        }
    }

    fun updateWeightType(type: ClientWeight.Type) {
        _clientWeight.update { clientWeight ->
            clientWeight.copy(type = type)
        }
    }
}
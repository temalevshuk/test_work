package com.example.uiclientcreateoredit.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.example.domainclient.entity.weight.ClientWeight
import com.example.uiclientcreateoredit.ClientDataPageFragment
import com.example.uiclientcreateoredit.ClientDataPageFragment.Companion.MOVE_TO_NEXT_PAGE_EXTRA
import com.example.uiclientcreateoredit.ClientDataPageFragment.Companion.MOVE_TO_NEXT_PAGE_REQUEST_KEY
import com.example.uiclientcreateoredit.ClientIdShareViewModel
import com.example.uiclientcreateoredit.R
import com.example.uiclientcreateoredit.databinding.FragmentClientWeightBinding
import com.example.uicommon.ktx.hideKeyboard
import com.example.uicommon.ktx.launchAndCollectIn
import org.koin.androidx.viewmodel.ext.android.viewModel

internal class ClientWeightFragment : Fragment() {

    private var _binding: FragmentClientWeightBinding? = null
    private val binding: FragmentClientWeightBinding
        get() = _binding!!

    private val viewModel: ClientWeightViewModel by viewModel()
    private val shareClientIdViewModel by viewModels<ClientIdShareViewModel>(
        ownerProducer = { requireParentFragment() }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentClientWeightBinding.inflate(
        inflater,
        container,
        false
    ).also { fragmentBinding ->
        _binding = fragmentBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shareClientIdViewModel.shareClientId.launchAndCollectIn(viewLifecycleOwner) { clientId ->
            clientId?.let(viewModel::loadClientWeight)
        }

        with(binding) {
            setFragmentResultListener(ClientDataPageFragment.SAVE_WEIGHT_REQUEST_KEY) { _, _ ->
                shareClientIdViewModel.shareClientId.value?.let { clientId ->
                    val weightValue = etWeight.text.toString()
                    if (weightValue.isNotBlank()) {
                        etWeight.hideKeyboard()
                        viewModel.saveClientWeight(clientId, etWeight.text.toString())
                        setFragmentResult(
                            MOVE_TO_NEXT_PAGE_REQUEST_KEY,
                            bundleOf(MOVE_TO_NEXT_PAGE_EXTRA to true)
                        )
                    } else {
                        etWeight.error = getString(com.example.uicommon.R.string.error_message)
                        setFragmentResult(
                            MOVE_TO_NEXT_PAGE_REQUEST_KEY,
                            bundleOf(MOVE_TO_NEXT_PAGE_EXTRA to false)
                        )
                    }
                }
            }

            viewModel.clientWeight.launchAndCollectIn(viewLifecycleOwner) { clientWeight ->
                etWeight.setText(clientWeight.value)
            }

            btnWeightTypePicker.setOnClickListener {
                val popupMenu = PopupMenu(requireContext(), btnWeightTypePicker)
                val inflater = popupMenu.menuInflater
                inflater.inflate(R.menu.menu_weight_popup, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.mWeightLb -> {
                            Toast.makeText(requireContext(), "LB", Toast.LENGTH_SHORT).show()
                            viewModel.updateWeightType(ClientWeight.Type.LB)
                            true
                        }
                        R.id.mWeightKg -> {
                            Toast.makeText(requireContext(), "KG", Toast.LENGTH_SHORT).show()
                            viewModel.updateWeightType(ClientWeight.Type.KG)
                            true
                        }
                        else -> super.onOptionsItemSelected(item)
                    }
                }
                popupMenu.show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.example.uiclientcreateoredit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domainclient.entity.ClientId
import com.example.domainclient.usecase.CreateClientUseCase
import com.example.domainclient.usecase.DeleteClientUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

internal class ClientDataViewModel(
    private val existClientId: Long,
    private val createClientUseCase: CreateClientUseCase,
    private val deleteClientUseCase: DeleteClientUseCase
) : ViewModel() {

    private val _clientId: MutableStateFlow<ClientId?> = MutableStateFlow(null)
    internal val clientId = _clientId.asStateFlow()

    init {
        if (existClientId <= -1L) {
            viewModelScope.launch {
                _clientId.update { createClientUseCase.invoke() }
            }
        } else {
            _clientId.update { ClientId(existClientId) }
        }
    }

    fun deleteClient() {
        viewModelScope.launch {
            deleteClientUseCase.invoke(
                clientId = checkNotNull(_clientId.value) { "Client identifier can't be null" },
            )
        }
    }

}
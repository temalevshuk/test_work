package com.example.uiclientcreateoredit

import androidx.lifecycle.ViewModel
import com.example.domainclient.entity.ClientId
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

internal class ClientIdShareViewModel: ViewModel() {
    private val _shareClientId: MutableStateFlow<ClientId?> = MutableStateFlow(null)
    val shareClientId = _shareClientId.asStateFlow()

    fun updateClientId(clientId: ClientId) {
        _shareClientId.update { clientId }
    }
}
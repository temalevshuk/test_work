package com.example.uiclientcreateoredit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.uiclientcreateoredit.databinding.FragmentClientDataPageBinding
import com.example.uiclientcreateoredit.dob.ClientDateOfBirthFragment
import com.example.uiclientcreateoredit.photo.ClientPhotoFragment
import com.example.uiclientcreateoredit.weight.ClientWeightFragment
import com.example.uicommon.ktx.initToolbar
import com.example.uicommon.ktx.launchAndCollectIn
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ClientDataPageFragment : Fragment() {

    private var _binding: FragmentClientDataPageBinding? = null
    private val binding: FragmentClientDataPageBinding
        get() = _binding!!

    private val viewModel: ClientDataViewModel by viewModel(
        parameters = {
            parametersOf(requireArguments().getLong(CLIENT_ID_EXTRA))
        }
    )
    private val shareClientIdViewModel by viewModels<ClientIdShareViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentClientDataPageBinding.inflate(
        inflater,
        container,
        false
    ).also { fragmentBinding ->
        _binding = fragmentBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.clientId.launchAndCollectIn(viewLifecycleOwner) { clientId ->
            clientId?.let(shareClientIdViewModel::updateClientId)
        }

        with(binding) {
            childFragmentManager.setFragmentResultListener(
                MOVE_TO_NEXT_PAGE_REQUEST_KEY,
                viewLifecycleOwner
            ) { _, bundle ->
                val toNext = bundle.getBoolean(MOVE_TO_NEXT_PAGE_EXTRA, false)
                if (toNext) {
                    val currentItem = vpDataPage.currentItem
                    if (currentItem != CONST_SCREEN_CHILD_COUNT - 1) {
                        vpDataPage.setCurrentItem(currentItem + 1, true)
                    } else {
                        findNavController().popBackStack()
                    }
                }
            }

            initToolbar(
                toolbar = toolbar,
                onBackPressed = {
                    back()
                }
            )

            with(vpDataPage) {
                adapter = object : FragmentStateAdapter(this@ClientDataPageFragment) {
                    override fun getItemCount(): Int = CONST_SCREEN_CHILD_COUNT

                    override fun createFragment(position: Int): Fragment = when (position) {
                        0 -> ClientWeightFragment()
                        1 -> ClientDateOfBirthFragment()
                        2 -> ClientPhotoFragment()
                        else -> throw RuntimeException("Wrong pager position $position")
                    }
                }
                isUserInputEnabled = false
                offscreenPageLimit = 1
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        when (position) {
                            0, 1 -> btnNext.text = getString(com.example.uicommon.R.string.next)
                            2 -> btnNext.text = getString(com.example.uicommon.R.string.done)
                        }
                    }
                })
            }
            TabLayoutMediator(tvDotsTab, vpDataPage) { _, _ -> }.attach()

            btnBack.setOnClickListener {
                back()
            }

            btnNext.setOnClickListener {
                when (vpDataPage.currentItem) {
                    0 -> {
                        childFragmentManager.setFragmentResult(
                            SAVE_WEIGHT_REQUEST_KEY,
                            bundleOf()
                        )
                    }
                    1 -> {
                        childFragmentManager.setFragmentResult(
                            SAVE_DATE_OF_BIRTH_REQUEST_KEY,
                            bundleOf()
                        )
                    }
                    2 -> {
                        childFragmentManager.setFragmentResult(
                            SAVE_PHOTO_REQUEST_KEY,
                            bundleOf()
                        )
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        binding.vpDataPage.adapter = null
        super.onDestroyView()
        _binding = null
    }

    private fun back() {
        with(binding) {
            val currentItem = vpDataPage.currentItem
            if (currentItem != 0) {
                vpDataPage.setCurrentItem(currentItem - 1, true)
            } else {
                if (requireArguments().getBoolean(CLIENT_CREATE_EXTRA, false)) {
                    viewModel.deleteClient()
                }
                findNavController().popBackStack()
            }
        }
    }

    companion object {
        private const val CONST_SCREEN_CHILD_COUNT = 3

        internal const val MOVE_TO_NEXT_PAGE_REQUEST_KEY = "MOVE_TO_NEXT_PAGE_REQUEST_KEY"
        internal const val MOVE_TO_NEXT_PAGE_EXTRA = "MOVE_TO_NEXT_PAGE_EXTRA"

        internal const val SAVE_WEIGHT_REQUEST_KEY = "SAVE_WEIGHT_REQUEST_KEY"
        internal const val SAVE_DATE_OF_BIRTH_REQUEST_KEY = "SAVE_DATE_OF_BIRTH_REQUEST_KEY"
        internal const val SAVE_PHOTO_REQUEST_KEY = "SAVE_PHOTO_REQUEST_KEY"

        const val CLIENT_ID_EXTRA = "CLIENT_ID_EXTRA"
        const val CLIENT_CREATE_EXTRA = "CLIENT_CREATE_EXTRA"
    }

}
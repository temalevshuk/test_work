package com.example.uiclientcreateoredit.dob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.example.uiclientcreateoredit.ClientDataPageFragment
import com.example.uiclientcreateoredit.ClientIdShareViewModel
import com.example.uiclientcreateoredit.databinding.FragmentClientDateOfBirthBinding
import com.example.uicommon.ktx.launchAndCollectIn
import org.koin.androidx.viewmodel.ext.android.viewModel

internal class ClientDateOfBirthFragment : Fragment() {

    private var _binding: FragmentClientDateOfBirthBinding? = null
    private val binding: FragmentClientDateOfBirthBinding
        get() = _binding!!

    private val viewModel: ClientDateOfBirthViewModel by viewModel()
    private val shareClientIdViewModel by viewModels<ClientIdShareViewModel>(
        ownerProducer = { requireParentFragment() }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentClientDateOfBirthBinding.inflate(
        inflater,
        container,
        false
    ).also { fragmentBinding ->
        _binding = fragmentBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shareClientIdViewModel.shareClientId.launchAndCollectIn(viewLifecycleOwner) { clientId ->
            clientId?.let(viewModel::loadClientBirthday)
        }
        with(binding) {
            viewModel.clientBirthday.launchAndCollectIn(viewLifecycleOwner) { clientBirthday ->
                dpPicker.updateDate(
                    clientBirthday.date.year,
                    clientBirthday.date.monthValue - 1,
                    clientBirthday.date.dayOfMonth
                )
            }
            setFragmentResultListener(ClientDataPageFragment.SAVE_DATE_OF_BIRTH_REQUEST_KEY) { _, _ ->
                shareClientIdViewModel.shareClientId.value?.let { clientId ->
                    viewModel.saveClientBirthday(
                        clientId,
                        dpPicker.dayOfMonth,
                        dpPicker.month,
                        dpPicker.year
                    )
                }
                setFragmentResult(
                    ClientDataPageFragment.MOVE_TO_NEXT_PAGE_REQUEST_KEY,
                    bundleOf(ClientDataPageFragment.MOVE_TO_NEXT_PAGE_EXTRA to true)
                )
            }
            dpPicker.maxDate = System.currentTimeMillis()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.example.uiclientcreateoredit.dob

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.usecase.get.GetClientDateOfBirthByIdUseCase
import com.example.domainclient.usecase.save.SaveClientDateOfBirthUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate

internal class ClientDateOfBirthViewModel(
    private val saveClientDateOfBirthUseCase: SaveClientDateOfBirthUseCase,
    private val getClientDateOfBirthByIdUseCase: GetClientDateOfBirthByIdUseCase
) : ViewModel() {

    private val _clientBirthday: MutableStateFlow<ClientBirthday> = MutableStateFlow(ClientBirthday())
    internal val clientBirthday = _clientBirthday.asStateFlow()

    fun loadClientBirthday(
        clientId: ClientId
    ) {
        viewModelScope.launch {
            getClientDateOfBirthByIdUseCase.invoke(clientId)?.let { clientBirthday ->
                _clientBirthday.update { clientBirthday }
            }
        }
    }

    fun saveClientBirthday(
        clientId: ClientId,
        dayOfMonth: Int,
        month: Int,
        year: Int
    ) {
        viewModelScope.launch {
            _clientBirthday.update { clientBirthday ->
                clientBirthday.copy(date = LocalDate.of(year, month + 1, dayOfMonth))
            }
            saveClientDateOfBirthUseCase.invoke(clientId, _clientBirthday.value)
        }
    }

}
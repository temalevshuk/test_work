package com.example.uiclientcreateoredit.photo

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.example.uiclientcreateoredit.ClientDataPageFragment
import com.example.uiclientcreateoredit.ClientIdShareViewModel
import com.example.uiclientcreateoredit.databinding.FragmentClientPhotoBinding
import com.example.uicommon.image.setImage
import com.example.uicommon.ktx.*
import org.koin.androidx.viewmodel.ext.android.viewModel

internal class ClientPhotoFragment : Fragment() {

    private var _binding: FragmentClientPhotoBinding? = null
    private val binding: FragmentClientPhotoBinding
        get() = _binding!!

    private val viewModel: ClientPhotoViewModel by viewModel()
    private val shareClientIdViewModel by viewModels<ClientIdShareViewModel>(
        ownerProducer = { requireParentFragment() }
    )

    private val takePhotoFromGallery = registerForTakePhotoGallery { uri ->
        viewModel.updatePhotoPreview(uri)
    }

    private val takeGalleryPermission = registerForCheckPermissions { isGranted ->
        if (isGranted) {
            takePhotoGallery(takePhotoFromGallery)
        } else {
            Toast.makeText(
                requireContext(),
                getString(com.example.uicommon.R.string.gallery_permission_denied),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private val takePhotoFromCamera = registerForTakePhotoCamera {
        viewModel.updatePhotoPreview(cameraImagePath)
    }

    private val takeCameraPermission = registerForCheckPermissions { isGranted ->
        if (isGranted) {
            takePhotoCamera(takePhotoFromCamera) { path ->
                cameraImagePath = path
            }
        } else {
            Toast.makeText(
                requireContext(),
                getString(com.example.uicommon.R.string.camera_permission_denied),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private var cameraImagePath = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentClientPhotoBinding.inflate(
        inflater,
        container,
        false
    ).also { fragmentBinding -> _binding = fragmentBinding }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shareClientIdViewModel.shareClientId.launchAndCollectIn(viewLifecycleOwner) { clientId ->
            clientId?.let(viewModel::loadClientPhoto)
        }
        setFragmentResultListener(ClientDataPageFragment.SAVE_PHOTO_REQUEST_KEY) { _, _ ->
            shareClientIdViewModel.shareClientId.value?.let { clientId ->
                viewModel.saveClientPhoto(clientId)
            }
            if (viewModel.photoPreview.value.path.value.isNotBlank()) {
                setFragmentResult(
                    ClientDataPageFragment.MOVE_TO_NEXT_PAGE_REQUEST_KEY,
                    bundleOf(ClientDataPageFragment.MOVE_TO_NEXT_PAGE_EXTRA to true)
                )
            }
        }

        with(binding) {
            viewModel.photoPreview.launchAndCollectIn(viewLifecycleOwner) { preview ->
                ivPhotoPreview.isVisible = preview.path.value.isNotBlank()
                ivPhotoPreview.setImage(preview.path.value)
            }

            btnPickFromGallery.setOnClickListener {
                takeGalleryPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            btnPickFromCamera.setOnClickListener {
                takeCameraPermission.launch(Manifest.permission.CAMERA)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
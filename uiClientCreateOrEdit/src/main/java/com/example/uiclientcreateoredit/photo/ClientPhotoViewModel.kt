package com.example.uiclientcreateoredit.photo

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.photo.ClientPhotoPath
import com.example.domainclient.usecase.get.GetClientPhotoByIdUseCase
import com.example.domainclient.usecase.save.SaveClientPhotoUseCase
import com.example.domainmedia.entity.MediaPath
import com.example.domainmedia.usecase.GetMediaPathFromContentUriUseCase
import com.example.domainmedia.usecase.GetRotatedMediaPathUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

internal class ClientPhotoViewModel(
    private val getRotatedMediaPathUseCase: GetRotatedMediaPathUseCase,
    private val getMediaPathFromContentUriUseCase: GetMediaPathFromContentUriUseCase,
    private val saveClientPhotoUseCase: SaveClientPhotoUseCase,
    private val getClientPhotoByIdUseCase: GetClientPhotoByIdUseCase
) : ViewModel() {

    private val _photoPreview: MutableStateFlow<ClientPhoto> = MutableStateFlow(ClientPhoto())
    internal val photoPreview = _photoPreview.asStateFlow()


    fun loadClientPhoto(
        clientId: ClientId
    ) {
        viewModelScope.launch {
            getClientPhotoByIdUseCase.invoke(clientId)?.let { clientPhoto ->
                _photoPreview.update { clientPhoto }
            }
        }
    }

    fun updatePhotoPreview(
        photoPath: String
    ) {
        viewModelScope.launch {
            val photo = getRotatedMediaPathUseCase.invoke(MediaPath(photoPath))
            _photoPreview.update { photoPreview -> photoPreview.copy(path = ClientPhotoPath(photo.value)) }
        }
    }

    fun updatePhotoPreview(
        photoUri: Uri
    ) {
        viewModelScope.launch {
            val photo = getMediaPathFromContentUriUseCase.invoke(photoUri)
            _photoPreview.update { photoPreview -> photoPreview.copy(path = ClientPhotoPath(photo.value)) }
        }
    }

    fun saveClientPhoto(
        clientId: ClientId
    ) {
        viewModelScope.launch {
            saveClientPhotoUseCase.invoke(clientId, _photoPreview.value)
        }
    }
}
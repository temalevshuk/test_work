package com.example.uiclientcreateoredit

import com.example.uiclientcreateoredit.dob.ClientDateOfBirthViewModel
import com.example.uiclientcreateoredit.photo.ClientPhotoViewModel
import com.example.uiclientcreateoredit.weight.ClientWeightViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val clientDataViewModelModule = module {
    viewModel { params ->
        ClientDataViewModel(
            existClientId = params.get(),
            createClientUseCase = get(),
            deleteClientUseCase = get()
        )
    }

    viewModel {
        ClientWeightViewModel(
            saveClientWeightUseCase = get(),
            getClientWeightByIdUseCase = get()
        )
    }

    viewModel {
        ClientDateOfBirthViewModel(
            saveClientDateOfBirthUseCase = get(),
            getClientDateOfBirthByIdUseCase = get()
        )
    }

    viewModel {
        ClientPhotoViewModel(
            getRotatedMediaPathUseCase = get(),
            getMediaPathFromContentUriUseCase = get(),
            saveClientPhotoUseCase = get(),
            getClientPhotoByIdUseCase = get()
        )
    }
}
package com.example.common

import android.content.Context
import java.io.File

fun Context.createTempFile(fileName: String): File {
    val dir = File(filesDir, "temp")
        .also { file ->
            if (!file.exists()) {
                file.mkdirs()
            }
        }
    return File(dir, fileName)
}
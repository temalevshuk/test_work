package com.example.uiclientmain.delegate

import androidx.recyclerview.widget.DiffUtil
import com.example.uiclientmain.model.ClientItemUIModel
import com.example.uicommon.adapter.RecyclerItem

internal class ClientDiffUtils : DiffUtil.ItemCallback<RecyclerItem>() {
    override fun areItemsTheSame(
        oldItem: RecyclerItem,
        newItem: RecyclerItem
    ) = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: RecyclerItem,
        newItem: RecyclerItem
    ): Boolean = oldItem == newItem

    override fun getChangePayload(
        oldItem: RecyclerItem,
        newItem: RecyclerItem
    ): Any? {
        return when {
            oldItem is ClientItemUIModel && newItem is ClientItemUIModel -> {
                if (oldItem.weight != newItem.weight ||
                    oldItem.dateOfBirth != newItem.dateOfBirth ||
                    oldItem.imagePath != newItem.imagePath
                ) newItem
                else super.getChangePayload(oldItem, newItem)
            }
            else -> super.getChangePayload(oldItem, newItem)
        }
    }
}
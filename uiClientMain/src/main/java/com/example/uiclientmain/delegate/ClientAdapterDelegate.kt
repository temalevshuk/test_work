package com.example.uiclientmain.delegate

import com.example.domainclient.entity.ClientId
import com.example.uiclientmain.databinding.ItemClientBinding
import com.example.uiclientmain.model.ClientItemUIModel
import com.example.uicommon.adapter.RecyclerItem
import com.example.uicommon.image.setImage
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

internal fun clientAdapterDelegate(
    onEditClick: (id: ClientId) -> Unit
) = adapterDelegateViewBinding<ClientItemUIModel, RecyclerItem, ItemClientBinding>(
    { layoutInflater, parent ->
        ItemClientBinding.inflate(layoutInflater, parent, false)
    }
) {
    binding.btnEdit.setOnClickListener {
        onEditClick.invoke(item.id)
    }

    bind { payloads ->
        if (payloads.isEmpty()) {
            item.update(binding)
        } else {
            val payload = payloads.first()
            check(payload is ClientItemUIModel) { "The item doesn't match the ClientItemUIModel type" }
            payload.update(binding)
        }
    }
}

private fun ClientItemUIModel.update(binding: ItemClientBinding) {
    with(binding) {
        tvWeightValue.text = weight
        tvDobValue.text = dateOfBirth
        ivPhoto.setImage(imagePath)
    }
}
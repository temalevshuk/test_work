package com.example.uiclientmain.delegate

import com.example.uiclientmain.databinding.ItemEmptyClientBinding
import com.example.uiclientmain.model.ClientEmptyItemUIModel
import com.example.uicommon.adapter.RecyclerItem
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

internal fun clientEmptyAdapterDelegate() =
    adapterDelegateViewBinding<ClientEmptyItemUIModel, RecyclerItem, ItemEmptyClientBinding>(
        { layoutInflater, parent ->
            ItemEmptyClientBinding.inflate(layoutInflater, parent, false)
        }
    ) {}
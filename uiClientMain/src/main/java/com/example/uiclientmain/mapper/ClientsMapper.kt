package com.example.uiclientmain.mapper

import android.text.format.DateFormat
import com.example.domainclient.entity.Client
import com.example.uiclientmain.model.ClientEmptyItemUIModel
import com.example.uiclientmain.model.ClientItemUIModel
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

internal fun List<Client>.mapToItemsUIModel() =
    if (isEmpty()) listOf(ClientEmptyItemUIModel)
    else map { client ->
        ClientItemUIModel(
            id = client.id,
            imagePath = client.photo.path.value,
            weight = "${client.weight.value}${client.weight.type.name}",
            dateOfBirth = client.dateOfBirthday.date.format(
                DateTimeFormatter.ofPattern(
                    DateFormat.getBestDateTimePattern(
                        Locale.US,
                        "dd MMMM yyyy"
                    ),
                    Locale.US
                )
            )
        )
    }
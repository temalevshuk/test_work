package com.example.uiclientmain

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domainclient.usecase.get.GetClientsUseCase
import com.example.uiclientmain.mapper.mapToItemsUIModel
import com.example.uicommon.adapter.RecyclerItem
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

internal class ClientMainViewModel(
    private val getClientsUseCase: GetClientsUseCase
) : ViewModel() {

    private val _clientsState: MutableStateFlow<List<RecyclerItem>> = MutableStateFlow(emptyList())
    internal val clientsState = _clientsState.asStateFlow()

    init {
        viewModelScope.launch {
            getClientsUseCase.invoke()
                .collect { clients ->
                    _clientsState.update {
                        clients.mapToItemsUIModel()
                    }
                }
        }
    }
}
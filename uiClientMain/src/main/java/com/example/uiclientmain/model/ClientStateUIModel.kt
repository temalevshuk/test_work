package com.example.uiclientmain.model

internal data class ClientStateUIModel(
    val isLoading: Boolean = true,
    val clients: List<String> = emptyList()
)
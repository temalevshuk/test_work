package com.example.uiclientmain.model

import com.example.uicommon.adapter.RecyclerItem

object ClientEmptyItemUIModel : RecyclerItem {
    override val id: Any
        get() = hashCode().toLong()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int = javaClass.hashCode()
}
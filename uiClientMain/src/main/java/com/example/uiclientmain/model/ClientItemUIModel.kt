package com.example.uiclientmain.model

import com.example.domainclient.entity.ClientId
import com.example.uicommon.adapter.RecyclerItem

internal data class ClientItemUIModel(
    override val id: ClientId,
    val imagePath: String,
    val weight: String,
    val dateOfBirth: String
): RecyclerItem
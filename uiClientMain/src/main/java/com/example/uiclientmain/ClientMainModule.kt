package com.example.uiclientmain

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val clientViewModelModule = module {
    viewModel {
        ClientMainViewModel(
            getClientsUseCase = get()
        )
    }
}
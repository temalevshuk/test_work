package com.example.uiclientmain

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.navigation.NavigationRouter
import com.example.uiclientmain.databinding.FragmentClientRootBinding
import com.example.uiclientmain.delegate.ClientDiffUtils
import com.example.uiclientmain.delegate.clientAdapterDelegate
import com.example.uiclientmain.delegate.clientEmptyAdapterDelegate
import com.example.uicommon.ktx.initToolbar
import com.example.uicommon.ktx.launchAndCollectIn
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ClientMainFragment : Fragment() {

    private var _binding: FragmentClientRootBinding? = null
    private val binding: FragmentClientRootBinding
        get() = _binding!!

    private val viewModel: ClientMainViewModel by viewModel()
    private val router: NavigationRouter by inject()

    private val adapter by lazy(LazyThreadSafetyMode.NONE) {
        AsyncListDifferDelegationAdapter(
            ClientDiffUtils(),
            clientAdapterDelegate { id ->
                router.toClientDataPage(
                    clientId = id.value,
                    isCreateClient = false
                )
            },
            clientEmptyAdapterDelegate()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentClientRootBinding.inflate(
        inflater,
        container,
        false
    ).also { fragmentBinding ->
        _binding = fragmentBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            viewModel.clientsState.launchAndCollectIn(viewLifecycleOwner) { clients ->
                adapter.items = clients
            }

            initToolbar(toolbar = toolbar, needSupportBack = false)
            rvClients.adapter = adapter
            btnAddClient.setOnClickListener {
                router.toClientDataPage()
            }
        }
    }

    override fun onDestroyView() {
        binding.rvClients.adapter = null
        super.onDestroyView()
        _binding = null
    }

}
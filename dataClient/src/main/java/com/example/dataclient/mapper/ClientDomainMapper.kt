package com.example.dataclient.mapper

import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity
import com.example.database.entity.relation.ClientFullData
import com.example.domainclient.entity.*
import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.entity.birthday.ClientBirthdayId
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.photo.ClientPhotoPath
import com.example.domainclient.entity.photo.ClientPhotoPathId
import com.example.domainclient.entity.weight.ClientWeight
import com.example.domainclient.entity.weight.ClientWeightId

internal fun List<ClientFullData>.mapToClients() = mapNotNull { data ->
    val weightEntity = data.weightEntity ?: return@mapNotNull null
    val birthdayEntity = data.birthdayEntity ?: return@mapNotNull null
    val photoEntity = data.photoEntity ?: return@mapNotNull null
    Client(
        id = ClientId(data.clientEntity.clientId),
        weight = weightEntity.mapToClientWeight(),
        dateOfBirthday = birthdayEntity.mapToClientBirthday(),
        photo = photoEntity.mapToClientPhotoPath()
    )
}

internal fun ClientWeightEntity.mapToClientWeight() = ClientWeight(
    id = ClientWeightId(clientWeightId),
    value = value,
    type = ClientWeight.Type.valueOf(type)
)

internal fun ClientBirthdayEntity.mapToClientBirthday() = ClientBirthday(
    id = ClientBirthdayId(clientBirthdayId),
    date = date
)

internal fun ClientPhotoEntity.mapToClientPhotoPath() = ClientPhoto(
    id = ClientPhotoPathId(clientPhotoId),
    path = ClientPhotoPath(photoPath)
)
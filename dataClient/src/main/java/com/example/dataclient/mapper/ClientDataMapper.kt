package com.example.dataclient.mapper

import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity
import com.example.domainclient.entity.ClientId
import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.weight.ClientWeight
import org.threeten.bp.LocalDate

internal fun mapToClientWeightEntity(
    clientId: ClientId,
    clientWeight: ClientWeight
) = ClientWeightEntity(
    clientWeightId = clientWeight.id.value,
    clientOwnerId = clientId.value,
    value = clientWeight.value,
    type = clientWeight.type.name
)

internal fun mapToClientBirthdayEntity(
    clientId: ClientId,
    birthday: ClientBirthday
) = ClientBirthdayEntity(
    clientBirthdayId = birthday.id.value,
    clientOwnerId = clientId.value,
    date = birthday.date
)

internal fun mapToClientPhotoEntity(
    clientId: ClientId,
    photo: ClientPhoto
) = ClientPhotoEntity(
    clientPhotoId = photo.id.value,
    clientOwnerId = clientId.value,
    photoPath = photo.path.value
)
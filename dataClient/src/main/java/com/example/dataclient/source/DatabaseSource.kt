package com.example.dataclient.source

import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity
import com.example.database.entity.relation.ClientFullData
import com.example.domainclient.entity.ClientId
import kotlinx.coroutines.flow.Flow

interface DatabaseSource {
    suspend fun deleteClient(clientId: ClientId)

    suspend fun saveClient(clientEntity: ClientEntity)
    suspend fun saveClientWeight(clientWeightEntity: ClientWeightEntity)
    suspend fun saveClientDateOfBirth(clientBirthdayEntity: ClientBirthdayEntity)
    suspend fun saveClientPhoto(clientPhotoEntity: ClientPhotoEntity)

    suspend fun getClients(): Flow<List<ClientFullData>>
    suspend fun getClientWeightById(clientId: ClientId): ClientWeightEntity?
    suspend fun getClientDateOfBirthById(clientId: ClientId): ClientBirthdayEntity?
    suspend fun getClientPhotoById(clientId: ClientId): ClientPhotoEntity?
}
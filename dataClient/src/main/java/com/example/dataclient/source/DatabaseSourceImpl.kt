package com.example.dataclient.source

import com.example.database.dao.ClientDao
import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity
import com.example.database.entity.relation.ClientFullData
import com.example.database.transaction.DbTransaction
import com.example.domainclient.entity.ClientId
import kotlinx.coroutines.flow.Flow

internal class DatabaseSourceImpl(
    private val dao: ClientDao,
    private val dbTransaction: DbTransaction
) : DatabaseSource {
    override suspend fun deleteClient(clientId: ClientId) {
        dbTransaction.inTransaction {
            with(dao) {
                deleteClientById(clientId.value)
                deleteClientWeightById(clientId.value)
                deleteClientBirthdayById(clientId.value)
                deleteClientPhotoById(clientId.value)
            }
        }
    }

    override suspend fun saveClient(clientEntity: ClientEntity) {
        dao.saveClientEntity(clientEntity)
    }

    override suspend fun saveClientWeight(clientWeightEntity: ClientWeightEntity) {
        dao.saveClientWeight(clientWeightEntity)
    }

    override suspend fun saveClientDateOfBirth(clientBirthdayEntity: ClientBirthdayEntity) {
        dao.saveClientDateOfBirth(clientBirthdayEntity)
    }

    override suspend fun saveClientPhoto(clientPhotoEntity: ClientPhotoEntity) {
        dao.saveClientPhoto(clientPhotoEntity)
    }

    override suspend fun getClients(): Flow<List<ClientFullData>> = dao.getClients()

    override suspend fun getClientWeightById(
        clientId: ClientId
    ): ClientWeightEntity? = dao.getClientWeightById(clientId.value)

    override suspend fun getClientDateOfBirthById(
        clientId: ClientId
    ): ClientBirthdayEntity? = dao.getClientDateOfBirthById(clientId.value)

    override suspend fun getClientPhotoById(
        clientId: ClientId
    ): ClientPhotoEntity? = dao.getClientPhotoPathById(clientId.value)
}
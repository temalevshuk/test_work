package com.example.dataclient.di

import com.example.dataclient.ClientRepositoryImpl
import com.example.dataclient.source.DatabaseSource
import com.example.dataclient.source.DatabaseSourceImpl
import com.example.domainclient.ClientRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val clientDataModule = module {
    single<DatabaseSource> { DatabaseSourceImpl(dao = get(), dbTransaction = get()) }
    single<ClientRepository> { ClientRepositoryImpl(databaseSource = get()) }
}
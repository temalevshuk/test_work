package com.example.dataclient

import com.example.database.dao.ClientDao
import com.example.database.entity.ClientEntity
import com.example.database.transaction.DbTransaction
import com.example.dataclient.mapper.*
import com.example.dataclient.mapper.mapToClientBirthdayEntity
import com.example.dataclient.mapper.mapToClientPhotoEntity
import com.example.dataclient.mapper.mapToClientWeightEntity
import com.example.dataclient.mapper.mapToClients
import com.example.dataclient.source.DatabaseSource
import com.example.domainclient.ClientRepository
import com.example.domainclient.entity.*
import com.example.domainclient.entity.birthday.ClientBirthday
import com.example.domainclient.entity.photo.ClientPhoto
import com.example.domainclient.entity.weight.ClientWeight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class ClientRepositoryImpl(
    private val databaseSource: DatabaseSource
) : ClientRepository {

    override suspend fun createClient(): ClientId = withContext(Dispatchers.IO) {
        val clientId = ClientId(System.currentTimeMillis())
        databaseSource.saveClient(ClientEntity(clientId = clientId.value))
        clientId
    }

    override suspend fun deleteClient(clientId: ClientId) {
        withContext(Dispatchers.IO) {
            databaseSource.deleteClient(clientId)
        }
    }

    override suspend fun saveClientWeight(clientId: ClientId, clientWeight: ClientWeight) {
        withContext(Dispatchers.IO) {
            databaseSource.saveClientWeight(mapToClientWeightEntity(clientId, clientWeight))
        }
    }

    override suspend fun saveClientDateOfBirth(
        clientId: ClientId,
        clientDateOfBirth: ClientBirthday
    ) = withContext(Dispatchers.IO) {
        databaseSource.saveClientDateOfBirth(mapToClientBirthdayEntity(clientId, clientDateOfBirth))
    }

    override suspend fun saveClientPhoto(
        clientId: ClientId,
        clientPhoto: ClientPhoto
    ) = withContext(Dispatchers.IO) {
        databaseSource.saveClientPhoto(mapToClientPhotoEntity(clientId, clientPhoto))
    }

    override suspend fun getClients(): Flow<List<Client>> = withContext(Dispatchers.IO) {
        databaseSource.getClients().map { clients ->
            clients.mapToClients()
        }
    }

    override suspend fun getClientWeightById(
        clientId: ClientId
    ): ClientWeight? = withContext(Dispatchers.IO) {
        databaseSource.getClientWeightById(clientId)?.mapToClientWeight()
    }

    override suspend fun getClientDateOfBirthById(
        clientId: ClientId
    ): ClientBirthday? = withContext(Dispatchers.IO) {
        databaseSource.getClientDateOfBirthById(clientId)?.mapToClientBirthday()
    }

    override suspend fun getClientPhotoById(
        clientId: ClientId
    ): ClientPhoto? = withContext(Dispatchers.IO) {
        databaseSource.getClientPhotoById(clientId)?.mapToClientPhotoPath()
    }
}
package com.example.database.transaction

interface DbTransaction {
    suspend fun <T> inTransaction(block: suspend () -> T): T
}
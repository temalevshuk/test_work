package com.example.database.transaction

import androidx.room.withTransaction
import com.example.database.Db

internal class DbTransactionImpl(
    private val db: Db
) : DbTransaction{
    override suspend fun <T> inTransaction(block: suspend () -> T): T = db.withTransaction(block)
}
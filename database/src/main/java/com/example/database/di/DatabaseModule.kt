package com.example.database.di

import android.content.Context
import androidx.room.Room
import com.example.database.Db
import com.example.database.dao.ClientDao
import com.example.database.transaction.DbTransaction
import com.example.database.transaction.DbTransactionImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    fun provideDatabase(applicationContext: Context): Db = Room.databaseBuilder(
        applicationContext,
        Db::class.java,
        "mnd_test.db"
    ).build()

    fun provideClientDao(db: Db): ClientDao = db.getClientDao()

    fun provideDbTransaction(db: Db): DbTransaction = DbTransactionImpl(db)

    single { provideDatabase(androidApplication()) }
    single { provideDbTransaction(get()) }
    factory { provideClientDao(get()) }
}
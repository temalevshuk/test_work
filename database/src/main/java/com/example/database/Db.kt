package com.example.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.database.dao.ClientDao
import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity

@Database(
    version = 1,
    entities = [
        ClientEntity::class,
        ClientWeightEntity::class,
        ClientBirthdayEntity::class,
        ClientPhotoEntity::class
    ]
)
abstract class Db : RoomDatabase() {
    abstract fun getClientDao(): ClientDao
}
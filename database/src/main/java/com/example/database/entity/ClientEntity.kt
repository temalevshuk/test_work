package com.example.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.database.entity.ClientEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ClientEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_CLIENT_ID)
    val clientId: Long
) {
    internal companion object {
        const val TABLE_NAME = "client"
        const val COLUMN_CLIENT_OWNER_ID = "client_owner_id"
        const val COLUMN_CLIENT_ID = "client_id"
    }
}
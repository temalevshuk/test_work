package com.example.database.entity.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity

data class ClientFullData(
    @Embedded val clientEntity: ClientEntity,

    @Relation(
        parentColumn = ClientEntity.COLUMN_CLIENT_ID,
        entityColumn = ClientEntity.COLUMN_CLIENT_OWNER_ID
    )
    val weightEntity: ClientWeightEntity?,

    @Relation(
        parentColumn = ClientEntity.COLUMN_CLIENT_ID,
        entityColumn = ClientEntity.COLUMN_CLIENT_OWNER_ID
    )
    val birthdayEntity: ClientBirthdayEntity?,

    @Relation(
        parentColumn = ClientEntity.COLUMN_CLIENT_ID,
        entityColumn = ClientEntity.COLUMN_CLIENT_OWNER_ID
    )
    val photoEntity: ClientPhotoEntity?
)
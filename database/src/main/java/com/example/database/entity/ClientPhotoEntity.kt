package com.example.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.database.entity.ClientEntity.Companion.COLUMN_CLIENT_OWNER_ID
import com.example.database.entity.ClientPhotoEntity.Companion.COLUMN_PHOTO_PATH
import com.example.database.entity.ClientPhotoEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ClientPhotoEntity(
    @PrimaryKey
    val clientPhotoId: Long,
    @ColumnInfo(name = COLUMN_CLIENT_OWNER_ID)
    val clientOwnerId: Long,
    @ColumnInfo(name = COLUMN_PHOTO_PATH)
    val photoPath: String
) {
    internal companion object {
        const val TABLE_NAME = "client_photo"
        const val COLUMN_PHOTO_PATH = "photo_path"
    }
}
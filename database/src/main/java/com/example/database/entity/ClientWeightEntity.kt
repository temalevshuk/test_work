package com.example.database.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import androidx.room.util.TableInfo
import com.example.database.entity.ClientEntity.Companion.COLUMN_CLIENT_OWNER_ID
import com.example.database.entity.ClientWeightEntity.Companion.COLUMN_CLIENT_WEIGHT_ID
import com.example.database.entity.ClientWeightEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ClientWeightEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_CLIENT_WEIGHT_ID)
    val clientWeightId: Long,

    @ColumnInfo(name = COLUMN_CLIENT_OWNER_ID)
    val clientOwnerId: Long,

    val value: String,
    val type: String
) {
    internal companion object {
        const val TABLE_NAME = "client_weight"
        const val COLUMN_CLIENT_WEIGHT_ID = "client_weight_id"
    }
}
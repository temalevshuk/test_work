package com.example.database.entity

import androidx.room.*
import com.example.database.converters.LocalDateToStringConverter
import com.example.database.entity.ClientBirthdayEntity.Companion.COLUMN_CLIENT_BIRTHDAY_ID
import com.example.database.entity.ClientBirthdayEntity.Companion.TABLE_NAME
import com.example.database.entity.ClientEntity.Companion.COLUMN_CLIENT_OWNER_ID
import org.threeten.bp.LocalDate

@Entity(tableName = TABLE_NAME)
@TypeConverters(LocalDateToStringConverter::class)
data class ClientBirthdayEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_CLIENT_BIRTHDAY_ID)
    val clientBirthdayId: Long,
    @ColumnInfo(name = COLUMN_CLIENT_OWNER_ID)
    val clientOwnerId: Long,
    val date: LocalDate
) {
    internal companion object {
        const val TABLE_NAME = "client_birthday"
        const val COLUMN_CLIENT_BIRTHDAY_ID = "client_birthday_id"
    }
}
package com.example.database.converters

import androidx.room.TypeConverter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class LocalDateToStringConverter {
    @TypeConverter
    fun toLocalData(
        string: String
    ): LocalDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("dd.MM.yyyy"))

    @TypeConverter
    fun fromLocalDate(
        localDate: LocalDate
    ): String = localDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
}
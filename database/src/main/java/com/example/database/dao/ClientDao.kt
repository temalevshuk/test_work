package com.example.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction
import com.example.database.entity.ClientBirthdayEntity
import com.example.database.entity.ClientEntity
import com.example.database.entity.ClientPhotoEntity
import com.example.database.entity.ClientWeightEntity
import com.example.database.entity.relation.ClientFullData
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ClientDao {

    @Transaction
    @Query("SELECT * FROM client")
    abstract fun getClients(): Flow<List<ClientFullData>>

    @Transaction
    @Query("SELECT * FROM client WHERE client_id=:clientId")
    abstract fun getClientById(clientId: Long): ClientFullData

    @Query("SELECT * FROM client_weight WHERE client_owner_id=:clientId")
    abstract fun getClientWeightById(clientId: Long): ClientWeightEntity?

    @Query("SELECT * FROM client_birthday WHERE client_owner_id=:clientId")
    abstract fun getClientDateOfBirthById(clientId: Long): ClientBirthdayEntity?

    @Query("SELECT * FROM client_photo WHERE client_owner_id=:clientId")
    abstract fun getClientPhotoPathById(clientId: Long): ClientPhotoEntity?

    @Insert(onConflict = REPLACE)
    abstract fun saveClientEntity(clientEntity: ClientEntity)

    @Insert(onConflict = REPLACE)
    abstract fun saveClientWeight(clientWeightEntity: ClientWeightEntity)

    @Insert(onConflict = REPLACE)
    abstract fun saveClientDateOfBirth(clientBirthdayEntity: ClientBirthdayEntity)

    @Insert(onConflict = REPLACE)
    abstract fun saveClientPhoto(clientPhotoEntity: ClientPhotoEntity)

    @Query("DELETE FROM client WHERE client_id=:clientId")
    abstract fun deleteClientById(clientId: Long)

    @Query("DELETE FROM client_weight WHERE client_owner_id=:clientId")
    abstract fun deleteClientWeightById(clientId: Long)

    @Query("DELETE FROM client_birthday WHERE client_owner_id=:clientId")
    abstract fun deleteClientBirthdayById(clientId: Long)

    @Query("DELETE FROM client_photo WHERE client_owner_id=:clientId")
    abstract fun deleteClientPhotoById(clientId: Long)
}